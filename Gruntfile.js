module.exports = function(grunt) {
    grunt.initConfig({
        browserify: {
            'dist/index.js': ['src/index.js'],
        },
        watch: {
            files: ["src/**/*.js"],
            tasks: ['browserify', 'uglify']
        },
        uglify: {
            my_target: {
                files: {
                    'dist/index.min.js': ['dist/index.js']
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
}