(function() {
    /**
     * This file implements the bootstrapping of the application using AngularJS
     */
    var angular = require('angular');

    var app = angular.module('flickr-application', [/* no dependencies */]);

    /**
     * Define application controllers
     */
    app.controller('PhotosController', require('./controllers/PhotosController'));

    /**
     * Define application factories
     */
    app.factory('FlickrFactory', require('./factories/FlickrFactory'));
})();