module.exports = ['$scope', 'FlickrFactory', '$timeout', '$q', function($scope, FlickrFactory, $timeout, $q) {

    /**
     * This object represents a query for flickr
     */
    $scope.pagination = {};
    $scope.pagination.page = 1;
    $scope.pagination.per_page = 20;
    $scope.pagination.extras = ['description', 'tags'].join(',');
    $scope.pagination.text = '';

    $scope.photosResponse = {};
    $scope.photos = [];

    /**
     * This method allows you to fetch photos from flickr
     * 
     * If pagination.text is empty recent photos will be downloaded
     * If pagination.text is not empty search will be called for flickr api
     * 
     * This method promise you the fetch will be completed in X time
     * 
     * @return promise
     */
    $scope.getPhotos = function() {
        var deferred = $q.defer();
        var options = {
            page: $scope.pagination.page,
            per_page: $scope.pagination.per_page
        };
        var hasSearch = false;

        if ($scope.pagination.text.match(/\S/)) {
            options.text = $scope.pagination.text;
            hasSearch = true;
        }

        FlickrFactory.photos[hasSearch ? 'search' : 'getRecent'](options, function(error, response) {
            if (error) {
                throw new Error(error);
            } else if (response.stat === 'ok') {
                $scope.photosResponse = response.photos;
                $scope.photos = $scope.photos.concat(response.photos.photo);

                //refresh the angular view after async task
                $timeout(function() {
                    $scope.$apply();
                    deferred.resolve();
                });
            }
        });

        return deferred.promise;
    };

    /**
     * This is used to prevent appending of queries
     */
    $scope.loadingInProgress = false;

    /**
     * This method allows you to load next page with pictures
     * If loadingInProgress is boolean true getPhotos will not be called
     * 
     * @returns undefined
     */
    $scope.next = function() {
        if ($scope.pagination.page < parseInt($scope.photosResponse.pages, 10) && !$scope.loadingInProgress) {
            $scope.loadingInProgress = true;
            $scope.pagination.page += 1;
            $scope.getPhotos().then(function() {
                $timeout(function() {
                    $scope.loadingInProgress = false;
                }, 100);
            });
        }
    };

    $scope.searchTyping = function() {
        $scope.pagination.page = 1;
        $scope.photos = [];
        $scope.next();
    };

    /**
     * This method is used to calculate
     *  if given element is in the viewport of the page
     *  
     * This is helper function for infinity scroll
     *  to determine when to call next function
     * 
     * @param {DOMElement} el - the dom element which you want to check for visibility
     * @returns Boolean true if visible false if not
     */
    $scope.isElementOutViewport = function(el) {
        var rect = el.getBoundingClientRect();
        return rect.bottom < 0 || rect.right < 0 || rect.left > window.innerWidth || rect.top > window.innerHeight;
    };

    var infinityEye = document.getElementById('infinity-eye');

    $scope.load = function() {
        //dont call next if the infinityEye is not in the viewport of the page
        if (!$scope.isElementOutViewport(infinityEye)) {
            $scope.next();
        }
    };

    //call load on every scroll event
    window.addEventListener('scroll', $scope.load);

    $scope.getPhotos();
}];