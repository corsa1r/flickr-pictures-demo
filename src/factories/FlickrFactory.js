/**
 * global Flickr
 */
var flickrOptions = require('../config/flickr-api');
require('flickrapi/browser/flickrapi.dev');

module.exports = [function() {
    return new Flickr(flickrOptions);
}];