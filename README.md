# README #

This application implements infinity scroll with search for images from Flickr

### Technologies ###
* HTML & CSS and responsive
* JavaScript
* Angular core framework
* Browserify 
* Grunt task runner
* Firebase (hosting only)

### Demo page ###

* [Demo](https://flickr-task.firebaseapp.com)
* Version 1

### How do I get set up? ###

* git clone
* npm install